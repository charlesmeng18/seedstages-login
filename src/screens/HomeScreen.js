import React, { memo } from 'react';
import Background from '../components/Background';
import Logo from '../components/Logo';
import Header from '../components/Header';
import Button from '../components/Button';
import Paragraph from '../components/Paragraph';

const HomeScreen = ({ navigation }) => (
  <Background>
    <Logo />
    <Header>Welcome to Seedstages</Header>

    <Paragraph>
      Find your Student-Startup Match Today!!
    </Paragraph>
    <Button mode="contained" onPress={() => navigation.navigate('RegisterScreen')}>
      Sign Up
    </Button>
    <Button
      mode="outlined"
      onPress={() => navigation.navigate('LoginScreen')}
    >
      Log In
    </Button>
  </Background>
);

export default memo(HomeScreen);
