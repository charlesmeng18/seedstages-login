import React, { memo } from 'react';
import Background from '../components/Background';
import Logo from '../components/Logo';
import Header from '../components/Header';
import Paragraph from '../components/Paragraph';
import Button from '../components/Button';

const Dashboard = ({ navigation }) => (
  <Background>
    <Logo />
    <Header>Let’s Begin with What Your Goals Are</Header>
    <Paragraph>
      Are you a recruiter or a student?
    </Paragraph>
    <Button mode="outlined" onPress={() => navigation.navigate('HomeScreen')}>
      Recruiter
    </Button>
    <Button mode="outlined" onPress={() => navigation.navigate('HomeScreen')}>
      Student
    </Button>
    <Button onPress={() => navigation.navigate('HomeScreen')}>
      Logout
    </Button>
  </Background>
);

export default memo(Dashboard);
