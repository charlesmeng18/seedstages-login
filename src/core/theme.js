import { DefaultTheme } from 'react-native-paper';

export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#66CDAA',
    secondary: '#414757',
    error: '#f13a59',
  },
};
